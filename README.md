# docker-min-tex

A basic docker image for latex.

## Usage

docker run --rm -i -u="$(id -u):$(id -g)" --net=none -v "$PWD":/data wrdeman/docker-min-tex pdflatex a-document.tex`
