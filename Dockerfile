FROM ubuntu:bionic
MAINTAINER Simon Osborne <wrdeplay@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q \
    && apt-get install --no-install-recommends -qy \ 
    libfontconfig1 \
    texlive-base \
    texlive-extra-utils \
    texlive-generic-recommended \
    texlive-fonts-recommended \
    texlive-font-utils \
    texlive-latex-base \
    texlive-latex-recommended \
    texlive-latex-extra \
    && rm -rf /var/lib/apt/lists/*

ENV HOME /data
WORKDIR /data
VOLUME ["/data"]


